﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.Material
{
    public class Supplier
    {
        public string SupplierID { get; set; }
        public string FullName { get; set; }
    }
}